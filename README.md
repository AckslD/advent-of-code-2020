# advent of code 2020

Code for [advent of code 2020](https://adventofcode.com/2020).

Each day has a seperate file `srs/dayxx.rs` with a dedicated `run`-function.
There are also additional shared files for the intcode-computer, graphs, numbers etc.

Disclaimer: The solutions of the different days have quite varying level of quality :)

Author: Axel Dahlberg
