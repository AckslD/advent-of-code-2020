use advent2019::toolbox::read_input;

fn p1() {
    let raw_str = read_input("data/day1.txt".to_string());
    let numbers: Vec<i32> = raw_str.split('\n').filter(|s| s.len() > 0).map(|s| s.parse().unwrap()).collect();
    for i in 0..(numbers.len()-1) {
        for j in (i+1)..(numbers.len()-1) {
            let a = numbers[i];
            let b = numbers[j];
            if a + b == 2020 {
                println!("{}", a * b);
            }
        }
    }
}

fn p2() {
    let raw_str = read_input("data/day1.txt".to_string());
    let numbers: Vec<i32> = raw_str.split('\n').filter(|s| s.len() > 0).map(|s| s.parse().unwrap()).collect();
    for i in 0..(numbers.len()-1) {
        for j in (i+1)..(numbers.len()-1) {
            for k in (j+1)..(numbers.len()-1) {
                let a = numbers[i];
                let b = numbers[j];
                let c = numbers[k];
                if a + b + c == 2020 {
                    println!("{}", a * b * c);
                }
            }
        }
    }
}

pub fn run() {
    p2();
}
