use advent2019::toolbox::read_input;

type Num = u64;

const DATA: &'static str = "data/day9.txt";
// Size of preamble
const N: usize = 25;

fn is_sum_of_two(number: Num, numbers: &[Num]) -> bool {
    let n = numbers.len();
    for i in 0..n {
        for j in (i+1)..n {
            if number == numbers[i] + numbers[j] {
                return true;
            }
        }
    }
    false
}

fn find_invalid_number(numbers: &Vec<Num>) -> Option<Num> {
    let n = numbers.len();
    for i in N..n {
        let number = numbers[i];
        if ! is_sum_of_two(number, &numbers[i-N..i]) {
            return Some(number);
        }
    }
    None
}

fn find_contiguous_set(invalid: Num, numbers: &Vec<Num>) -> Option<Vec<Num>> {
    let n = numbers.len();
    for i in 0..(n-1) {
        for j in (i+1)..n {
            let set = &numbers[i..(j+1)];
            if invalid == set.iter().sum() {
                return Some(set.iter().map(|d| *d).collect());
            }
        }
    }
    None
}

pub fn run() {
    let raw_str = read_input(DATA.to_string());
    let numbers: Vec<Num> = raw_str.split('\n')
                                   .filter(|s| s.len() > 0)
                                   .map(|s| s.parse().unwrap())
                                   .collect();
    let invalid = find_invalid_number(&numbers).unwrap();
    println!("First invalid number is {}", invalid);

    let set = find_contiguous_set(invalid, &numbers).unwrap();
    let smallest = set.iter().min().unwrap();
    let largest = set.iter().max().unwrap();
    println!("Smallest {} + largest {} = {}", smallest, largest, smallest+largest);
}
