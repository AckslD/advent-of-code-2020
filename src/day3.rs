use std::collections::HashSet;
use advent2019::toolbox::read_input;

type Position = (usize, usize);

#[derive(Debug)]
struct Forest {
    trees: HashSet<Position>,
    width: usize,
    height: usize,
}

type Slope = (usize, usize);

struct ForestIterator {
    pos: Position,
    width: usize,
    height: usize,
    slope: Slope,
}

impl Iterator for ForestIterator {
    type Item = Position;

    fn next(&mut self) -> Option<Self::Item> {
        if self.pos.0 >= self.height {
            None
        } else {
            let curr_pos = (self.pos.0, self.pos.1);
            self.pos.0 += self.slope.0;
            self.pos.1 += self.slope.1;
            self.pos.1 %= self.width;
            Some(curr_pos)
        }
    }
}

impl Forest {
    fn traverse(&self, slope: Slope) -> ForestIterator {
        ForestIterator{
            pos: (0, 0),
            width: self.width,
            height: self.height,
            slope,
        }
    }

    fn has_tree(&self, pos: Position) -> bool {
        self.trees.contains(&pos)
    }
}

impl From<String> for Forest {
    fn from(s: String) -> Self {
        let mut height = 0;
        let mut width = 0;
        let mut trees = HashSet::new();
        for row in s.split('\n') {
            if row.len() == 0 {
                continue;
            }
            width = 0;
            for c in row.chars() {
                match c {
                    '#' => {trees.insert((height, width));}
                    '.' => {}
                    _ => {panic!("Unknown tile {}", c);}
                }
                width += 1;
            }
            height += 1;
        }

        Forest{
            trees,
            width,
            height,
        }
    }
}

fn count_trees(slope: Slope) -> usize {
    let raw_str = read_input("data/day3.txt".to_string());
    let forest = Forest::from(raw_str);
    forest.traverse(slope)
          .filter(|p| forest.has_tree(*p))
          .count()
}

pub fn run() {
    let product: usize = [(1, 1), (1, 3), (1, 5), (1, 7), (2, 1)].iter()
                                                          .map(|s| count_trees(*s))
                                                          .product();

    println!("{:?}", product);
}
