use std::collections::HashSet;
use advent2019::toolbox::read_input;

fn get_seat_ids() -> HashSet<isize> {
    let raw_str = read_input("data/day5.txt".to_string());
    let mut seat_ids = HashSet::new();
    for entry in raw_str.split('\n') {
        if entry.len() == 0 {
            continue;
        }
        // println!("{}", entry);
        let (row, seat) = entry.split_at(7);
        let row_bin = row.replace('F', "0").replace('B', "1");
        let seat_bin = seat.replace('L', "0").replace('R', "1");
        // println!("{}, {}", row_bin, seat_bin);
        let row = isize::from_str_radix(&row_bin, 2).unwrap();
        let seat = isize::from_str_radix(&seat_bin, 2).unwrap();
        // println!("{}, {}", row, seat);
        let seat_id = row * 8 + seat;
        // println!("{}", seat_id);
        seat_ids.insert(seat_id);
    }
    seat_ids
}

fn get_largest_seat_id(seat_ids: &HashSet<isize>) -> isize {
    let mut largest = 0;
    for seat_id in seat_ids {
        if *seat_id > largest {
            largest = *seat_id;
        }
    }
    largest
}

pub fn p1() {
    let seat_ids = get_seat_ids();
    let largest = get_largest_seat_id(&seat_ids);
    println!("Largest seat ID: {}", largest);
}

pub fn p2() {
    let seat_ids = get_seat_ids();
    let largest = get_largest_seat_id(&seat_ids);
    for seat_id in 1..largest {
        if ! seat_ids.contains(&seat_id) &&
             seat_ids.contains(&(seat_id-1)) &&
             seat_ids.contains(&(seat_id+1)) {
            println!("Your seat id is: {}", seat_id);
            return;
        }
    }
    panic!("Couldn't find your seat id");
}

pub fn run() {
    // p1();
    p2();
}
