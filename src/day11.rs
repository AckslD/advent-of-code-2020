use std::collections::HashMap;
use advent2019::toolbox::read_input;
use advent2020::misc::Problem;

// const DATA: &'static str = "data/day11_0.txt";
// // const VERSION: Problem = Problem::One;
// const VERSION: Problem = Problem::Two;

#[derive(Clone, Copy, PartialEq, Eq)]
enum Tile {
    Empty,
    Occupied,
    Floor,
}

impl From<char> for Tile {
    fn from (c: char) -> Tile {
        match c {
            'L' => Tile::Empty,
            '#' => Tile::Occupied,
            '.' => Tile::Floor,
            _ => {panic!("Unknown tile {}", c);}
        }
    }
}

struct Board {
    tiles: Vec<Vec<Tile>>,
    height: usize,
    width: usize,
    version: Problem,
}

impl From<(Problem, &str)> for Board {
    fn from (inp: (Problem, &str)) -> Board {
        let mut tiles = Vec::new();
        for row_str in inp.1.split('\n') {
            if row_str.len() == 0 {
                continue;
            }
            let mut row: Vec<Tile> = Vec::new();
            for c in row_str.chars() {
                row.push(c.into());
            }
            tiles.push(row);
        }
        let height = tiles.len();
        let width = tiles[0].len();
        Board{
            tiles,
            height,
            width,
            version: inp.0,
        }
    }
}

impl Board {
    // Returns num changes
    fn evolve(&mut self) -> usize {
        let mut changes: HashMap<(usize, usize), Tile> = HashMap::new();
        for i in 0..self.height {
            for j in 0..self.width {
                match self.tiles[i][j] {
                    Tile::Empty => {
                        if self.occupied_neighbours(i, j) == 0 {changes.insert((i,j), Tile::Occupied);}
                    }
                    Tile::Occupied => {
                        let n = match self.version {
                            Problem::One => {4}
                            Problem::Two => {5}
                        };
                        if self.occupied_neighbours(i, j) >= n {changes.insert((i,j), Tile::Empty);}
                    }
                    Tile::Floor => {}
                }
            }
        }
        let num_changes = changes.len();
        // Apply changes
        for ((i, j), tile) in changes {
            self.tiles[i][j] = tile;
        }
        num_changes
    }

    fn occupied_neighbours(&self, row: usize, col: usize) -> u8 {
        // let above = if row == 0 {0} else {row-1};
        // let below = if row == self.height-1 {self.height-1} else {row+1};
        // let left = if col == 0 {0} else {col-1};
        // let right = if col == self.width-1 {self.width-1} else {col+1};
        let row = row as isize;
        let col = col as isize;
        let mut count = 0;
        for i in -1..2 {
            for j in -1..2 {
                if i == 0 && j == 0 {
                    continue; // skip specified cell
                }
                let mut steps = 1;
                loop {
                    let other_row = row + steps * i;
                    let other_col = col + steps * j;
                    if other_row < 0 || other_row >= self.height as isize || other_col < 0 || other_col >= self.width as isize {
                        break; // outside board
                    }
                    match self.version {
                        Problem::One => {
                            if self.tiles[other_row as usize][other_col as usize] == Tile::Occupied {
                                count += 1;
                            }
                            break; // break after one step
                        }
                        Problem::Two => {
                            match self.tiles[other_row as usize][other_col as usize] {
                                Tile::Empty => {break;}
                                Tile::Occupied => {count += 1; break;}
                                Tile::Floor => {}
                            }
                        }
                    }
                    steps += 1;
                }
            }
        }
        count
    }

    fn total_occupied(&self) -> usize {
        let mut count = 0;
        for i in 0..self.height {
            for j in 0..self.width {
                if self.tiles[i][j] == Tile::Occupied {
                    count += 1;
                }
            }
        }
        count
    }
}

fn compute_seats(version: Problem, file: &str) -> usize {
    let raw_str = read_input(file.to_string());
    let mut board = Board::from((version, &raw_str[..]));
    let mut num_changes = 1;
    while num_changes != 0 {
        num_changes = board.evolve();
        // println!("Num changes: {}", num_changes);
        // println!("Seats occupied: {}", board.total_occupied());
    }
    board.total_occupied()
}

pub fn run() {
    println!("Problem 1:");
    let seats = compute_seats(Problem::One, "data/day11.txt");
    println!("Seats occupied: {}", seats);

    println!("Problem 2:");
    let seats = compute_seats(Problem::Two, "data/day11.txt");
    println!("Seats occupied: {}", seats);
}

#[test]
fn test() {
    assert_eq!(compute_seats(Problem::One, "data/day11_0.txt"), 37);
    assert_eq!(compute_seats(Problem::Two, "data/day11_0.txt"), 26);
}
