use advent2019::toolbox::read_input;

const DATA: &'static str = "data/day10.txt";

type Num = u64;

fn get_diff_sequence() -> Vec<Num> {
    let raw_str = read_input(DATA.to_string());
    let mut numbers: Vec<Num> = raw_str.split('\n')
                                   .filter(|s| s.len() > 0)
                                   .map(|s| s.parse().unwrap())
                                   .collect();
    numbers.sort();
    let highest = numbers[numbers.len()-1];
    numbers.insert(0, 0);
    numbers.push(highest+3);
    // println!("{:?}", numbers);
    let mut diffs: Vec<Num> = Vec::new();
    for (i, n) in numbers.iter().enumerate() {
        let diff: Num;
        if i == numbers.len()-1 {
            break;
        }
        diff = numbers[i+1] - n;
        diffs.push(diff);
    }
    diffs
}

fn p1() {
    println!("Problem 1");
    let diffs = get_diff_sequence();
    let n1 = diffs.iter().filter(|d| **d == 1).count();
    let n3 = diffs.iter().filter(|d| **d == 3).count();
    println!("Answer: {}*{} = {}", n1, n3, n1 * n3);
}

fn p2() {
    println!("Problem 2");
    let diffs = get_diff_sequence();
    // println!("{:?}", diffs);
    let combinations: Num = diffs.split(|d| *d == 3)
                            .map(|ones| ones.iter().count())
                            .map(|n| match n {
                                0 => {1}
                                1 => {1}
                                2 => {2}
                                3 => {4}
                                4 => {7}
                                _ => {panic!("To large sequence {}", n);}
                            })
                            .product();
    println!("Answer: {}", combinations);
}

pub fn run() {
    p1();
    p2();
}
