use advent2019::toolbox::read_input;

type Password = String;

#[derive(Debug)]
struct Policy {
    letter: char,
    min: usize,
    max: usize,
}

#[derive(Debug)]
struct Entry {
    policy: Policy,
    password: Password,
}

#[derive(Clone, Copy)]
enum Problem {
    One,
    Two,
}

impl Entry {
    fn is_valid(&self, version: Problem) -> bool {
        match version {
            Problem::One => {
                let n = self.password.chars().filter(|c| *c == self.policy.letter).count();
                self.policy.min <= n && n <= self.policy.max
            },
            Problem::Two => {
                let chars: Vec<char> = self.password.chars().collect();
                (chars[self.policy.min-1] == self.policy.letter) ^ (chars[self.policy.max-1] == self.policy.letter)
            },
        }
    }
}

impl From<&str> for Entry {
    fn from(s: &str) -> Self {
        let parts: Vec<&str> = s.split(' ').collect();
        assert_eq!(parts.len(), 3);
        let range: Vec<usize> = parts[0].split('-').map(|s| s.parse().unwrap()).collect();
        assert_eq!(range.len(), 2);
        let letter = parts[1].chars().next().unwrap();
        let password = parts[2].to_string();
        Entry{
            policy: Policy{
                letter,
                min: range[0],
                max: range[1],
            },
            password,
        }
    }
}

pub fn run() {
    // let version = Problem::One;
    let version = Problem::Two;
    let raw_str = read_input("data/day2.txt".to_string());
    let count = raw_str.split('\n')
                       .filter(|s| s.len() > 0)
                       .map(|s| Entry::from(s))
                       .filter(|e| e.is_valid(version))
                       .count();
    println!("Valid entries: {}", count);
}
