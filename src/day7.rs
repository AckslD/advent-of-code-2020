use std::collections::HashMap;
use advent2019::toolbox::read_input;

type Color = String;
type Content = HashMap<Color, usize>;
type BagRules = HashMap<Color, Content>;
type ContainCache = HashMap<Color, HashMap<Color, bool>>;
type NumCache = HashMap<Color, usize>;


fn parse_item(s: &str) -> (Color, usize) {
    let fields: Vec<&str> = s.split(' ').collect();
    let count: usize = fields[0].parse().unwrap();
    let color = fields[1..].join(" ");
    (color, count)
}

fn get_bag_contents() -> BagRules {
    let raw_str = read_input("data/day7.txt".to_string());
    let mut bags = HashMap::new();
    for row in raw_str.split('\n') {
        if row.len() == 0 {
            continue;
        }
        let rule: Vec<&str> = row.split(" bags contain ").collect();
        assert_eq!(rule.len(), 2);
        let bag = rule[0].to_string();
        let content_str = rule[1].replace(".", "").replace("no other bags", "").replace(" bags", "").replace(" bag", "");
        let content: Content = content_str.split(", ")
                                          .filter(|s| s.len() > 0)
                                          .map(|s| parse_item(s))
                                          .collect();
        // println!("{}: {:?}", bag, content);
        bags.insert(bag, content);
    }
    bags
}

fn has_bag(container: &Color, bag: &Color, cache: &mut ContainCache, bags: &BagRules) -> bool {
    match cache.get(container) {
        Some(subbags) => {
            match subbags.get(bag) {
                Some(contains) => {return *contains;}
                None => {}
            }
        }
        None => {}
    }
    // not in cache
    let mut contains = false;
    for subbag in bags.get(container).unwrap().keys() {
        if subbag == bag || has_bag(subbag, bag, cache, bags) {
            contains = true;
            break
        }
    }
    cache.entry(container.to_string()).or_insert(HashMap::new()).insert(bag.to_string(), contains);
    contains
}

fn num_subbags(container: &Color, cache: &mut NumCache, bags: &BagRules) -> usize {
    match cache.get(container) {
        Some(n) => {
            return *n;
        }
        None => {}
    }
    // not in cache
    let mut n = 0;
    for (subbag, count) in bags.get(container).unwrap() {
        n += count * (1 + num_subbags(subbag, cache, bags));
    }
    cache.insert(container.to_string(), n);
    n
}

fn p1() {
    let bags = get_bag_contents();
    let mut cache = ContainCache::new();
    let mut count = 0;
    for bag in bags.keys() {
        if has_bag(bag, &"shiny gold".to_string(), &mut cache, &bags) {
            count += 1
        }
    }
    println!("count = {}", count);
}

fn p2() {
    let bags = get_bag_contents();
    let mut cache = NumCache::new();
    let count = num_subbags(&"shiny gold".to_string(), &mut cache, &bags);
    println!("count = {}", count);
}

pub fn run() {
    p1();
    p2();
}
