use advent2019::toolbox::read_input;

#[derive(Debug)]
enum Instruction {
    North(u8),
    South(u8),
    East(u8),
    West(u8),
    Rot90(u8),
    Forward(u8),
}

impl From<&str> for Instruction {
    fn from(s: &str) -> Instruction {
        let (instr, num) = s.split_at(1);
        match instr {
            "N" => {Instruction::North(num.parse().unwrap())}
            "S" => {Instruction::South(num.parse().unwrap())}
            "E" => {Instruction::East(num.parse().unwrap())}
            "W" => {Instruction::West(num.parse().unwrap())}
            "R" => {
                match num {
                    "90" => {Instruction::Rot90(1)}
                    "180" => {Instruction::Rot90(2)}
                    "270" => {Instruction::Rot90(3)}
                    _ => {panic!("Unknown instruction {}", s);}
                }
            }
            "L" => {
                match num {
                    "90" => {Instruction::Rot90(3)}
                    "180" => {Instruction::Rot90(2)}
                    "270" => {Instruction::Rot90(1)}
                    _ => {panic!("Unknown instruction {}", s);}
                }
            }
            "F" => {Instruction::Forward(num.parse().unwrap())}
            _ => {panic!("Unknown instruction {}", s);}
        }
    }
}

#[derive(Debug)]
enum Direction {
    North,
    South,
    East,
    West,
}

impl Direction {
    fn rot90(&self) -> Direction {
        match self {
            Direction::North => {Direction::East}
            Direction::South => {Direction::West}
            Direction::East => {Direction::South}
            Direction::West => {Direction::North}
        }
    }
}

#[derive(Debug)]
struct Boat {
    x: i32,
    y: i32,
    dir: Direction,
}

impl Boat {
    fn new() -> Boat {
        Boat{x: 0, y: 0, dir: Direction::East}
    }

    fn mv(&mut self, instr: &Instruction) {
        match instr {
            Instruction::North(n) => {self.y += *n as i32;}
            Instruction::South(n) => {self.y -= *n as i32;}
            Instruction::East(n) => {self.x += *n as i32;}
            Instruction::West(n) => {self.x -= *n as i32;}
            Instruction::Rot90(n) => {for _ in 0..*n {self.dir = self.dir.rot90()}}
            Instruction::Forward(n) => {
                match self.dir {
                    Direction::North => {self.mv(&Instruction::North(*n));}
                    Direction::South => {self.mv(&Instruction::South(*n));}
                    Direction::East => {self.mv(&Instruction::East(*n));}
                    Direction::West => {self.mv(&Instruction::West(*n));}
                }
            }
        }
    }
}

#[derive(Debug)]
struct Boat2 {
    x: i32,
    y: i32,
    wx: i32,
    wy: i32,
}

impl Boat2 {
    fn new() -> Boat2 {
        Boat2{x: 0, y: 0, wx: 10, wy: 1}
    }

    fn mv(&mut self, instr: &Instruction) {
        match instr {
            Instruction::North(n) => {self.wy += *n as i32;}
            Instruction::South(n) => {self.wy -= *n as i32;}
            Instruction::East(n) => {self.wx += *n as i32;}
            Instruction::West(n) => {self.wx -= *n as i32;}
            Instruction::Rot90(n) => {for _ in 0..*n {self.wrot90()}}
            Instruction::Forward(n) => {
                self.x += (*n as i32) * self.wx;
                self.y += (*n as i32) * self.wy;
            }
        }
    }
    
    fn wrot90(&mut self) {
        let wy = -self.wx;
        let wx = self.wy;
        self.wx = wx;
        self.wy = wy;
    }
}

fn p1(data: &str) {
    let raw_str = read_input(data.to_string());
    let instructions: Vec<Instruction> = raw_str.split('\n')
                                                .filter(|s| s.len() > 0)
                                                .map(|s| s.into())
                                                .collect();
    let mut boat = Boat::new();
    for instr in instructions {
        boat.mv(&instr);
    }
    println!("{:?}", boat);
    println!("Manhattan distance: {}", boat.x.abs() + boat.y.abs());
}

fn p2(data: &str) {
    let raw_str = read_input(data.to_string());
    let instructions: Vec<Instruction> = raw_str.split('\n')
                                                .filter(|s| s.len() > 0)
                                                .map(|s| s.into())
                                                .collect();
    let mut boat = Boat2::new();
    for instr in instructions {
        boat.mv(&instr);
        // println!("after {:?} - {:?}", instr, boat);
    }
    println!("{:?}", boat);
    println!("Manhattan distance: {}", boat.x.abs() + boat.y.abs());
}

pub fn run() {
    let data = "data/day12.txt";
    println!("Problem 1:");
    p1(data);
    println!("Problem 2:");
    p2(data);
}
