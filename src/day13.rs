use advent2019::toolbox::read_input;

pub fn run() {
    let raw_str = read_input("data/day13.txt".to_string());
    let lines: Vec<&str> = raw_str.split('\n').filter(|s| s.len() > 0).collect();
    assert_eq!(lines.len(), 2);
    let num: u64 = lines[0].parse().unwrap();
    let times: Vec<u64> = lines[1].split(',').filter(|s| s != &"x").map(|s| s.parse().unwrap()).collect();
    let mut wait = 0;
    loop {
        for time in &times {
            if (num + wait) % *time == 0 {
                println!("Wait time {} for bus {}", wait, time);
                println!("Answer = {}", wait * time);
                return;
            }
        }
        wait += 1;
    }
}
