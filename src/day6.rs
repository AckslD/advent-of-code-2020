use std::collections::HashSet;
use advent2019::toolbox::read_input;
use advent2020::misc::Problem;


pub fn run() {
    // let version = Problem::One;
    let version = Problem::Two;
    let raw_str = read_input("data/day6.txt".to_string());
    let mut count = 0;
    let mut group_answers: HashSet<char>;
    for group_str in raw_str.split("\n\n") {
        match version {
            Problem::One => {
                group_answers = HashSet::new();
            }
            Problem::Two => {
                group_answers = (b'a'..=b'z').map(char::from).collect();
            }
        }
        // println!("g={:?}", group_answers);
        for person_str in group_str.split('\n') {
            if person_str.len() == 0 {
                continue;
            }
            let person_answer: HashSet<char> = person_str.chars().collect();
            // println!("p={:?}", person_answer);
            match version {
                Problem::One => {
                    group_answers = group_answers.union(&person_answer).map(|c| *c).collect();
                }
                Problem::Two => {
                    group_answers = group_answers.intersection(&person_answer).map(|c| *c).collect();
                }
            }

        }
        // println!("{:?}", group_answers);
        count += group_answers.len();
    }
    println!("Sum of counts = {}", count);
}
