use std::collections::HashMap;
use advent2019::toolbox::read_input;

struct Passport {
    fields: HashMap<String, String>,
}

impl From<&str> for Passport {
    fn from(s: &str) -> Passport {
        let mut fields = HashMap::new();
        for f in s.replace('\n', " ").split(' ') {
            if f.len() == 0 {
                continue;
            }
            let kv_pair: Vec<&str> = f.split(':').collect();
            assert_eq!(kv_pair.len(), 2);
            fields.insert(kv_pair[0].to_string(), kv_pair[1].to_string());
        }
        Passport{fields}
    }
}

const REQ_FIELDS: [&'static str; 7] = [
    "byr",
    "iyr",
    "eyr",
    "hgt",
    "hcl",
    "ecl",
    "pid",
    // "cid",
];

#[derive(Clone, Copy)]
enum Problem {
    One,
    Two,
}

impl Passport {
    fn is_valid(&self, version: Problem) -> bool {
        match version {
            Problem::One => {
                for rf in REQ_FIELDS.iter() {
                    if ! self.fields.contains_key(&rf.to_string()) {
                        return false;
                    }
                }
                true
            }
            Problem::Two => {
                // byr, iyr, eyr
                for (f, min, max) in [("byr", 1920, 2002), ("iyr", 2010, 2020), ("eyr", 2020, 2030)].iter() {
                    match self.fields.get(&f.to_string()) {
                        Some(val_str) => {
                            match val_str.parse::<u32>() {
                                Ok(val) => {
                                    if *min > val || val > *max {
                                        return false;
                                    }
                                }
                                _ => {return false;}
                            }
                        }
                        None => {return false;}
                    }
                }
                // hgt
                match self.fields.get("hgt") {
                    Some(val_str) => {
                        let (n_str, unit) = val_str.split_at(val_str.len()-2);
                        if unit == "cm" {
                            match n_str.parse::<u32>() {
                                Ok(n) => {
                                    if 150 > n || n > 193 {
                                        return false;
                                    }
                                }
                                _ => {return false;}
                            }
                        } else if unit == "in" {
                            match n_str.parse::<u32>() {
                                Ok(n) => {
                                    if 59 > n || n > 76 {
                                        return false;
                                    }
                                }
                                _ => {return false;}
                            }
                        } else {
                            return false;
                        }
                    }
                    None => {return false;}
                }
                // hcl
                match self.fields.get("hcl") {
                    Some(val_str) => {
                        if ! val_str.starts_with('#') {
                            return false;
                        }
                        for c in val_str.chars().skip(1) {
                            match c {
                                'a'..='z' | '0'..='9' => {}
                                _ => {return false;}
                            }
                        }
                    }
                    None => {return false;}
                }
                // ecl
                match self.fields.get("ecl") {
                    Some(val_str) => {
                        let mut valid_col = false;
                        for col in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].iter() {
                            if val_str == col {
                                valid_col = true;
                            }
                        }
                        if ! valid_col {
                            return false;
                        }
                    }
                    None => {return false;}
                }
                // pid
                match self.fields.get("pid") {
                    Some(val_str) => {
                        if val_str.len() != 9 {
                            return false;
                        }
                        match val_str.parse::<u32>() {
                            Ok(_) => {}
                            _ => {return false;}
                        }
                    }
                    None => {return false;}
                }
                true
            }
        }
    }
}

pub fn run() {
    // let problem = Problem::One;
    let problem = Problem::Two;
    let raw_str = read_input("data/day4.txt".to_string());
    let count = raw_str.split("\n\n")
                       .filter(|s| s.len() > 0)
                       .map(|s| Passport::from(s))
                       .filter(|p| p.is_valid(problem))
                       .count();
    println!("{}", count);
}
