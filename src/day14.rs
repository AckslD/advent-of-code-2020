use std::collections::HashMap;
use std::fmt;
use core::num::ParseIntError;
use itertools::Itertools;
use num::Num;
use advent2019::toolbox::read_input;
use advent2020::misc::Problem;

#[derive(Debug)]
struct Mask {
    zeros: Vec<usize>,
    ones: Vec<usize>,
}

impl From<&str> for Mask {
    fn from(s: &str) -> Mask{
        let sides: Vec<&str> = s.split(" = ").collect();
        assert_eq!(sides.len(), 2);
        assert!(sides[0] == "mask");
        let mut zeros = Vec::new();
        let mut ones = Vec::new();
        for (i, b) in sides[1].chars().enumerate() {
            match b {
                '0' => {zeros.push(i)}
                '1' => {ones.push(i)}
                'X' => {}
                _ => {panic!("Unknown char {}", b);}
            }
        }
        Mask{zeros, ones}
    }
}

#[derive(Debug)]
struct Assign {
    pos: usize,
    val: u64,
}

impl From<&str> for Assign {
    fn from(s: &str) -> Assign {
        let sides: Vec<&str> = s.split(" = ").collect();
        assert_eq!(sides.len(), 2);
        assert!(sides[0].starts_with("mem["));
        let pos: usize = sides[0].replace("mem[", "").replace("]", "").parse().unwrap();
        let val: u64 = sides[1].parse().unwrap();
        Assign{pos, val}
    }
}

#[derive(Debug)]
enum Instruction {
    Mask(Mask),
    Assign(Assign),
}

impl From<&str> for Instruction {
    fn from(s: &str) -> Instruction {
        if s.starts_with("mask") {
            Instruction::Mask(Mask::from(s))
        } else {
            Instruction::Assign(Assign::from(s))
        }
    }
}

#[derive(Debug)]
struct Memory {
    values: HashMap<usize, u64>,
    mask: Option<Mask>,
    version: Problem,
}

fn to_bin<T>(n: T) -> Vec<char>
    where T: fmt::Binary + fmt::Debug
    {
    format!("{:036b}", n).chars().collect()
}

fn from_bin<T>(chars: &Vec<char>) -> T
    where <T as Num>::FromStrRadixErr: fmt::Debug,
          T: Num,
    {
    T::from_str_radix(&chars.into_iter().collect::<String>(), 2).unwrap()
}

impl Memory {
    fn new(version: Problem) -> Memory {
        Memory{values: HashMap::new(), mask: None, version}
    }
    
    fn apply(&mut self, instr: Instruction) {
        match instr {
            Instruction::Mask(mask) => {self.mask = Some(mask);}
            Instruction::Assign(assign) => {
                match &self.mask {
                    Some(mask) => {
                        match self.version {
                            Problem::One => {
                                let mut val_bins = to_bin::<u64>(assign.val);
                                for i in &mask.zeros {
                                    val_bins[*i] = '0';
                                }
                                for i in &mask.ones {
                                    val_bins[*i] = '1';
                                }
                                self.values.insert(assign.pos, from_bin::<u64>(&val_bins));
                            }
                            Problem::Two => {
                                let mut floating_indices = Vec::new();
                                for i in 0..36 {
                                    if ! (mask.zeros.contains(&i) || mask.ones.contains(&i)) {
                                        floating_indices.push(i);
                                    }
                                }
                                // println!("assign.pos = {}", assign.pos);
                                // println!("{:?}", floating_indices.iter().map(|i| vec!(('0', i), ('1', i))).collect::<Vec<Vec<(char, &usize)>>>());
                                for elements in floating_indices.iter().map(|i| vec!(('0', i), ('1', i))).multi_cartesian_product() {
                                    // println!("el = {:?}", elements);
                                    let mut pos_bins = to_bin::<usize>(assign.pos);
                                    // println!("before pos_bins = {}", pos_bins.iter().collect::<String>());
                                    // for i in &mask.zeros {
                                    //     pos_bins[*i] = '0';
                                    // }
                                    for i in &mask.ones {
                                        pos_bins[*i] = '1';
                                    }
                                    for (b, i) in &elements {
                                        pos_bins[**i] = *b;
                                    }
                                    // println!("zeros = {:?}", mask.zeros);
                                    // println!("ones = {:?}", mask.ones);
                                    // println!("elements = {:?}", elements);
                                    // println!(" after pos_bins = {}", pos_bins.iter().collect::<String>());
                                    self.values.insert(from_bin::<usize>(&pos_bins), assign.val);
                                }
                            }
                        }
                    }
                    None => {panic!("No mask set");}
                }
            }
        }
    }
}

pub fn run() {
    for version in [Problem::One, Problem::Two].iter() {
        println!("Problem {:?}:", version);
        let raw_str = read_input("data/day14.txt".to_string());
        let instructions: Vec<Instruction> = raw_str.split('\n').filter(|s| s.len() > 0).map(|s| Instruction::from(s)).collect();
        // println!("{:?}", instructions);
        let mut memory = Memory::new(*version);
        for instr in instructions {
            memory.apply(instr);
            // println!("{:?}", memory);
        }
        let sum: u64 = memory.values.values().sum();
        println!("Sum = {}", sum);
    }
}
