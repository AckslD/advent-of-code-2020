use std::convert::TryInto;
use std::collections::HashSet;
use advent2019::toolbox::read_input;

#[derive(Clone, Copy)]
enum Instruction {
    Nop(isize),
    Acc(isize),
    Jmp(isize),
}

impl From<&str> for Instruction {
    fn from(s: &str) -> Instruction {
        let parts: Vec<&str> = s.split(' ').collect();
        assert_eq!(parts.len(), 2);
        let op = parts[0];
        let arg: isize = parts[1].parse().unwrap();
        match op {
            "nop" => {Instruction::Nop(arg)}
            "acc" => {Instruction::Acc(arg)}
            "jmp" => {Instruction::Jmp(arg)}
            _ => {panic!("Unknown instruction {} {}", op, arg);}
        }
    }
}

struct Program {
    instrs: Vec<Instruction>
}

impl From<&str> for Program {
    fn from(s: &str) -> Program {
        Program{
            instrs: s.split('\n').filter(|s| s.len() > 0).map(|s| Instruction::from(s)).collect(),
        }
    }
}

#[derive(Debug)]
struct Computer {
    instr_index: usize,
    accumulator: isize,
    coverage: HashSet<usize>,
}

impl Computer {
    fn new() -> Computer {
        Computer{
            instr_index: 0,
            accumulator: 0,
            coverage: HashSet::new(),
        }
    }

    fn execute_program(&mut self, program: &Program) -> (ExitStatus, isize) {
        while self.instr_index < program.instrs.len() {
            // println!("i={}, a={}", self.instr_index, self.accumulator);
            if self.coverage.contains(&self.instr_index) {
                // println!("repeating line {}", self.instr_index);
                return (ExitStatus::NoHalt, self.accumulator);
            }
            self.coverage.insert(self.instr_index);
            match program.instrs[self.instr_index] {
                Instruction::Nop(_) => {}
                Instruction::Acc(n) => {self.accumulator += n;}
                Instruction::Jmp(n) => {self.instr_index = ((self.instr_index as isize) + n-1).try_into().unwrap();}
            }
            self.instr_index += 1;
        }
        (ExitStatus::Finished, self.accumulator)
    }
}

#[derive(PartialEq, Eq)]
enum ExitStatus {
    Finished,
    NoHalt,
}

fn p1() {
    println!("Problem 1:");
    let raw_str = read_input("data/day8.txt".to_string());
    let program = Program::from(&raw_str[..]);
    let mut computer = Computer::new();
    let (_, accumulator) = computer.execute_program(&program);
    println!("Accumulator when repeating: {}", accumulator);
}

fn p2() {
    println!("Problem 2:");
    let raw_str = read_input("data/day8.txt".to_string());
    let original_program = Program::from(&raw_str[..]);
    for (i, instr) in original_program.instrs.iter().enumerate() {
        let mut mutated_program = Program::from(&raw_str[..]);
        match instr {
            Instruction::Nop(n) => {mutated_program.instrs[i] = Instruction::Jmp(*n);}
            Instruction::Acc(_) => {}
            Instruction::Jmp(n) => {mutated_program.instrs[i] = Instruction::Nop(*n);}
        }
        let mut computer = Computer::new();
        let (exit_status, accumulator) = computer.execute_program(&mutated_program);
        if exit_status == ExitStatus::Finished {
            println!("Changed line {}", i);
            println!("Accumulator when finished: {}", accumulator);
            return;
        }
    }
    panic!("Couldn't fix the program");
}

pub fn run() {
    p1();
    p2();
}
